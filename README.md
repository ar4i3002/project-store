# Dumplings-store aka Пельменная №2
Это дипломный проект по курсу Яндекс практикума:DevOps для эксплуатации и разработки

https://dumplings.site/

<img width="900" alt="image" src="https://user-images.githubusercontent.com/9394918/167876466-2c530828-d658-4efe-9064-825626cc6db5.png">

## Frontend Backend ручная сборка
```bash
Frontend:
npm install
NODE_ENV=production VUE_APP_API_URL=http://localhost:8081 npm run serve
Backend:
go run ./cmd/api
go test -v ./... 
```
## Структура сервиса
 
[![Структура-сборки][1]][1]
 
[1]: https://s38vla.storage.yandex.net/rdisk/7bc4ab756b5a4e05b85099d86d175f5a83d1b98ee62723903ab445b959d900b6/637ba500/1d6h-5x5MRa4WEWRg8W6HQHFeX0Lk-OnpxW94WObalppZ03cmTbMEeldZpH75y9v3UCkIlo-y4pUm0qleMGHGg==?uid=550966216&filename=project-pipline.png&disposition=inline&hash=&limit=0&content_type=image%2Fpng&owner_uid=550966216&fsize=192087&hid=13d6285d0ac8aa742b79c25ae73e995c&media_type=image&tknv=v2&etag=e86c68d75e4eb3491cb6855666903dbf&rtoken=qEVjCxWRK3cY&force_default=yes&ycrid=na-072ef2b83c1f58860f988384184a51c1-downloader8h&ts=5edfd6a734000&s=1e533a9233a6c065b87f031c282a5751e3007c415fff99c8064faec3eb4ad3c0&pb=U2FsdGVkX1_hkOHsR9mGkdiuTkb57w_5_uBFVCz1G1CRanN_i-SmAWI9BwMCNEzvslMHOWoW8iqF2-nmzjO_Uxu7mubja2i9q9pTIx6hDTM
 
</details>

```
Gitlab структура 
- test тестирования сервиса
- main основна ветка разработки
- release 
```
## Dumplings-infrastructure 
https://gitlab.com/ar4i3002/dumplings-infrastructure

